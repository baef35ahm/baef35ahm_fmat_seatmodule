package com.baesystems.module.seat;

/**
 * Hello world!
 *
 */
public class App 
{
	private static final String HELLO_WORLD = "Hello, World!";
    public static void main( String[] args )
    {
        doThis();
    }
    
    public static void doThis() {
    	 System.out.println( HELLO_WORLD );
    }
}
